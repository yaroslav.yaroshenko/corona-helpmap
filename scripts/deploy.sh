#!/usr/bin/env bash

mvn clean package

echo 'Copy files...'


scp  -i ~/.ssh/id_rsa \
    target/corona-help-map-app-1.0-SNAPSHOT.jar \
    ubuntu@34.203.246.187:/home/ubuntu/

echo 'Restart server'


ssh -i ~/.ssh/id_rsa ubuntu@34.203.246.187 << EOF

pgrep java | xargs kill -9
nohup java -jar corona-help-map-app-1.0-SNAPSHOT.jar > log.txt &

EOF

echo 'Bye'