package ua.yaroshenko.springapp.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ua.yaroshenko.springapp.model.Location;

import java.util.List;

public interface LocationRepository extends JpaRepository<Location, Long> {
    Page<Location> findAll(Pageable pageable);
    Page<Location> findByUser_Username(String username, Pageable pageable);
}
