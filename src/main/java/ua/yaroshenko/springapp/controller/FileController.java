package ua.yaroshenko.springapp.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import ua.yaroshenko.springapp.model.User;
import ua.yaroshenko.springapp.service.UserService;

import java.io.IOException;

@Controller
public class FileController {

    @Value("${upload.path}")
    private String uploadPath;

    private final UserService userService;

    public FileController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(path = "/add-file")
    public ModelAndView addFile(@RequestParam("file") MultipartFile file,
                                @AuthenticationPrincipal User user) throws IOException
    {
        ModelAndView mv = new ModelAndView();
        User currentUser = userService.findById(user.getId());
        if (userService.saveFile(file, user))
        {
            mv.addObject("filename", user.getFilename());
            mv.addObject("user", currentUser);
            mv.addObject("uploadPath", uploadPath);
        }

        mv.setViewName("file");

        return mv;
    }

    @GetMapping("/file")
    public ModelAndView file(@AuthenticationPrincipal User user)
    {
        ModelAndView mv = new ModelAndView();
        User currentUser = userService.findById(user.getId());
        mv.addObject("user", currentUser);
        mv.addObject("uploadPath", uploadPath);
        mv.setViewName("file");
        return mv;
    }
    @PostMapping("/file")
    public ModelAndView openFilePagePost(@AuthenticationPrincipal User user)
    {
        ModelAndView mv = new ModelAndView();
        User currentUser = userService.findById(user.getId());
        mv.addObject("user", currentUser);
        mv.addObject("uploadPath", uploadPath);
        mv.setViewName("file");
        return mv;
    }

}
