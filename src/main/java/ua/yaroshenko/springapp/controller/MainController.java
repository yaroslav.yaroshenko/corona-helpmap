package ua.yaroshenko.springapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import ua.yaroshenko.springapp.model.Location;
import ua.yaroshenko.springapp.model.User;
import ua.yaroshenko.springapp.model.dto.CaptchaResponseDto;
import ua.yaroshenko.springapp.service.LocationService;
import ua.yaroshenko.springapp.service.UserService;

import javax.validation.Valid;
import java.io.IOException;
import java.util.*;


@Controller
public class MainController {
    private final static String CAPTCHA_URL = "https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s";

    @Value("${recaptcha.secret}")
    private String secret;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${home.greeting}")
    private String greeting;


    private final UserService userService;
    private final LocationService locationService;

    public MainController(UserService userService,LocationService locationService) {
        this.userService = userService;
        this.locationService = locationService;
    }

    @GetMapping("/add-user")
    public ModelAndView registration()
    {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("register-user");
        return mv;
    }





    @GetMapping("/activate/{code}")
    public ModelAndView activate(@PathVariable String code) {
        ModelAndView mv = new ModelAndView();
        boolean isActivated = userService.activateUser(code);

        if (isActivated) {
            mv.addObject("messageType", "success");
            mv.addObject("message", "User successfully activated");
        } else {
            mv.addObject("messageType", "danger");
            mv.addObject("message", "Activation code is not found!");
        }

        mv.setViewName("login");
        return mv;
    }

    @PostMapping(path = "/add-user")
    public ModelAndView addUser(
            @RequestParam("password2") String passwordConfirm,
            @RequestParam("g-recaptcha-response") String captchaResponce,
            @Valid User user,
            BindingResult bindingResult)
    {
        ModelAndView mv = new ModelAndView();

        String url = String.format(CAPTCHA_URL, secret, captchaResponce);
        CaptchaResponseDto response = restTemplate.postForObject(url, Collections.emptyList(), CaptchaResponseDto.class);

        boolean isConfirmEmpty = StringUtils.isEmpty(passwordConfirm);
        boolean isPasswordDifferent = user.getPassword() != null && !user.getPassword().equals(passwordConfirm);
        boolean isCaptchaResponseSuccess = response.isSuccess();
        if (!isCaptchaResponseSuccess) {
            mv.addObject("captchaError", "Fill captcha");
        }

        if (isConfirmEmpty) {
            mv.addObject("password2Error", "Password confirmation cannot be empty");
        }

        if (isPasswordDifferent) {
            mv.addObject("passwordError", "Passwords are different!");
        }

         if (bindingResult.hasErrors() || isConfirmEmpty || isPasswordDifferent || !isCaptchaResponseSuccess) {
             Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

             mv.addAllObjects(errorsMap);
         }else
         {
             if (userService.userIsExist(user.getUsername()))
             {
                 mv.addObject("userError", "User already exists!");

             }
             else
             {
                 userService.addUser(user);
                 mv.setViewName("redirect:/login");
                 return mv;
             }
         }
        mv.addObject("user", user);
        mv.setViewName("register-user");
        return mv;
    }

    @GetMapping("/admin")
    public ModelAndView admin()
    {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("admin");

        return mv;
    }

    @GetMapping("/403")
    public ModelAndView Error403()
    {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("403");

        return mv;
    }

    @GetMapping("/user")
    public ModelAndView user()
    {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("user");

        return mv;
    }

    @RequestMapping("/login")
    public ModelAndView login()
    {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("login");
        return mv;
    }

    @RequestMapping("/location")
    public ModelAndView location()
    {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("location");
        return mv;
    }


    @GetMapping("/profile")
    public ModelAndView profile()
    {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("profile-page");
        return mv;
    }

    @PostMapping("/profile")
    public ModelAndView saveProfile( @AuthenticationPrincipal User user,
                                      @RequestParam String password,
                                      @RequestParam String email
    ){
        User user1 = userService.findById(user.getId());
        userService.updateProfile(user1, password, email);
        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/home");
        return mv;
    }

    @GetMapping("/home")
        public ModelAndView homePageGet(@AuthenticationPrincipal User user,
                                        @RequestParam(required = false, defaultValue = "") String filter,
                                        @PageableDefault(sort = { "id"}, direction =Sort.Direction.DESC) Pageable pageable)
    {
        ModelAndView mv = new ModelAndView();
        User userFromDb = userService.findByUsername(user.getUsername());
        mv.addObject("username", userFromDb.getUsername());
        Page <Location> page;

        if (StringUtils.isEmpty(filter))
            page = locationService.findAll(pageable);
        else
            page = locationService.findUserByUsername(filter, pageable);

        mv.addObject("page", page);
        mv.addObject("elementsOnPageArray", Arrays.asList(5,10,25,50));
        mv.setViewName("home");
        return mv;
    }


    @GetMapping("/user-locations/{user}")
    public ModelAndView locationsList(@AuthenticationPrincipal User currentUser,
                                      @PathVariable User user,
                                      @RequestParam(required = false) Location location)
    {

        Set<Location> locations = user.getLocations();

        ModelAndView mv = new ModelAndView();
        mv.setViewName("locationsList");
        mv.addObject("userChannel", user);
        mv.addObject("subscriptionsCount", user.getSubscriptions().size());
        mv.addObject("subscribersCount", user.getSubscribers().size());
        mv.addObject("locations", locations);
        mv.addObject("location", location);
        mv.addObject("isCurrentUser", currentUser.equals(user));
        mv.addObject("isSubscriber", user.getSubscribers().contains(currentUser));

        return mv;
    }

    @PostMapping("/user-locations/{user}")
    public ModelAndView updateLocation(@AuthenticationPrincipal User currentUser,
                                      @PathVariable Long user,
                                      @RequestParam("id") Location location,
                                     @RequestParam("latitude") String latitude,
                                     @RequestParam("longitude") String longitude,
                                     @RequestParam("dimension") String dimension,
                                     @RequestParam("radius") Double radius)
    {
        ModelAndView mv = new ModelAndView();
       if(location.getUser().equals(currentUser))
       {
           if(!StringUtils.isEmpty(latitude))
           {
               location.setLatitude(latitude);
           }
           if(!StringUtils.isEmpty(longitude))
           {
               location.setLongitude(longitude);
           }
           if(!StringUtils.isEmpty(dimension))
           {
               location.setDimension(dimension);
           }
           if (radius > 0.0)
           {
               location.setRadius(radius);
           }
            locationService.saveLocation(location);
       }

        mv.setViewName("redirect:/user-locations/" + user);
        return mv;
    }

    @GetMapping("/user/subscribe/{user}")
    public ModelAndView subscribe(
            @AuthenticationPrincipal User currentUser,
            @PathVariable User user
    ) {
        ModelAndView mv = new ModelAndView();

        userService.subscribe(currentUser, user);

        mv.setViewName("redirect:/user-locations/" + user.getId());
        return mv;
    }
    @GetMapping("/user/unsubscribe/{user}")
    public ModelAndView unsubscribe(
            @AuthenticationPrincipal User currentUser,
            @PathVariable User user
    ) {
        ModelAndView mv = new ModelAndView();

        userService.unsubscribe(currentUser, user);

        mv.setViewName("redirect:/user-locations/" + user.getId());
        return mv;
    }

    @GetMapping("/user/{type}/{user}/list")
    public ModelAndView userList(
            @PathVariable User user,
            @PathVariable String type
    ) {
        ModelAndView mv = new ModelAndView();
        mv.addObject("userChannel", user);
        mv.addObject("type", type);

        if ("subscriptions".equals(type)) {
            mv.addObject("users", user.getSubscriptions());
        } else {
            mv.addObject("users", user.getSubscribers());
        }
        mv.setViewName("subscriptions");
        return mv;
    }
}
