package ua.yaroshenko.springapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ua.yaroshenko.springapp.model.Location;
import ua.yaroshenko.springapp.model.User;
import ua.yaroshenko.springapp.service.LocationService;

import javax.validation.Valid;
import java.util.*;

@Controller
public class LocationController {

    @Autowired
    private LocationService locationService;

    @PostMapping(path = "/add-location")
    public ModelAndView addLocation(@AuthenticationPrincipal User user,
                              @Valid Location location,
                              BindingResult bindingResult)
    {
        ModelAndView mv = new ModelAndView();
        if (bindingResult.hasErrors()) {
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

            mv.addAllObjects(errorsMap);
            mv.addObject("location" , location);

        }else {
            location.setUser(user);
            locationService.saveLocation(location);
            mv.addObject("location" , null);
        }
        mv.setViewName("redirect:/home");
        return mv;
    }

}

