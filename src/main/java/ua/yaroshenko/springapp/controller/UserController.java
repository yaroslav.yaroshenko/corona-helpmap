package ua.yaroshenko.springapp.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ua.yaroshenko.springapp.model.User;
import ua.yaroshenko.springapp.service.UserService;

import java.util.List;
import java.util.Map;

@Controller

public class UserController {

private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/users")
    public ModelAndView getUsersList(@RequestParam(required = false, defaultValue = "") String filter)
    {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("users");

        List <User> usersList = userService.getUsersList(filter);

        mv.addObject("users", usersList);
        mv.addObject("filter", filter);
        return mv;
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping(path = "/users/{id}")
    public ModelAndView getUserById(@PathVariable(value = "id") Long id) {

        ModelAndView mv = new ModelAndView();
        mv.setViewName("save-user");

        User user = userService.findById(id);

        mv.addObject("user", user);

        return mv;
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping(path = "/deleteUser/{id}")
    public ModelAndView deleteUserById(@PathVariable(value = "id") Long id) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/home");

        userService.deleteUserById(id);
        return mv;
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping(path ="/save-user")
            public ModelAndView saveUser(@RequestParam(name = "username") String username,
                                     @RequestParam Map<String, String> form)
    {
        User userFromDb = userService.findByUsername(username);
        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/home");

        userService.saveUser(userFromDb, form);
        return mv;

    }
}

