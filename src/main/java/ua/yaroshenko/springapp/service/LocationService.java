package ua.yaroshenko.springapp.service;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ua.yaroshenko.springapp.model.Location;
import ua.yaroshenko.springapp.repository.LocationRepository;

import java.util.List;

@Service
public class LocationService {

    private final LocationRepository locationRepository;

    public LocationService(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    public void saveLocation(Location location) {
        locationRepository.save(location);
    }

    public Page<Location> findAll(Pageable pageable) {
        return locationRepository.findAll(pageable);
    }

    public Page<Location> findUserByUsername(String filter, Pageable pageable) {
        return locationRepository.findByUser_Username(filter, pageable);
    }
}
