package ua.yaroshenko.springapp.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import ua.yaroshenko.springapp.model.Role;
import ua.yaroshenko.springapp.model.User;
import ua.yaroshenko.springapp.repository.UserRepository;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService implements UserDetailsService {
    @Value("${upload.path}")
    private String uploadPath;

    @Value("${hostname}")
    private String hostname;

    private final UserRepository userRepository;
    private final MailSender mailSender;
    private final PasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository, MailSender mailSender, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.mailSender = mailSender;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }

        return user;
    }

    public boolean userIsExist(String username)
    {
        User userFromDb = userRepository.findByUsername(username);

        if (userFromDb != null) return true;
        return false;
    }

    public void addUser(User user)
    {
        user.setEnabled(true);
        user.setRoles(Collections.singleton(Role.USER));
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setActivationCode(UUID.randomUUID().toString());

        userRepository.save(user);

        sendMessage(user);
    }

    private void sendMessage(User user) {
        if (!StringUtils.isEmpty(user.getEmail())) {
            String message = String.format(
                    "Hello, %s! \n" +
                            "Welcome to Corona HelpMap app. Please, visit next link: http://%s/activate/%s",
                    user.getUsername(),
                    hostname,
                    user.getActivationCode()
            );

            mailSender.send(user.getEmail(), "Activation code", message);
        }
    }

    public boolean activateUser(String code) {
        User user = userRepository.findByActivationCode(code);

        if (user == null) {
            return false;
        }

        user.setActivationCode(null);

        userRepository.save(user);

        return true;
    }

    public List<User> getUsersList(String filter) {
        List<User> usersList;

        if (filter != null && !filter.isEmpty()) {
            usersList = userRepository.findByUsernameList(filter);
        } else {
            usersList = userRepository.findAll();
        }

        return usersList;
    }

    public User findById(Long id) {
        Optional <User> user = userRepository.findById(id);

        return user.get();
    }

    public void deleteUserById(Long id) {

        userRepository.delete(userRepository.findById(id).get());
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public void saveUser(User user, Map<String, String> form) {

        //collecting to set roles from Role class
        Set<String> roles = Arrays.stream(Role.values())
                .map(Role::name)
                .collect(Collectors.toSet());

        user.getRoles().clear();

        //collection selected roles from form
        for (String key : form.keySet()) {
            if (roles.contains(key)) {
                user.getRoles().add(Role.valueOf(key));
            }
        }

        userRepository.save(user);

    }
    public boolean saveFile(MultipartFile file, User currentUser) throws IOException
    {
        if (!file.isEmpty())
        {
            File uploadDir  = new File(uploadPath);

                if (!uploadDir.exists()) uploadDir.mkdir();

            String uuidFile = UUID.randomUUID().toString();
            String resultFilename = uuidFile + "." + file.getOriginalFilename();

            file.transferTo(new File(uploadPath + "/" + resultFilename));
            currentUser.setFilename(resultFilename);
            userRepository.save(currentUser);
            return true;
        }
        return false;
    }

    public void updateProfile(User user, String password, String email) {
        String userEmail = user.getEmail();
        // if user changed to new unique mail
        boolean isEmailChanged = (email != null && !email.equals(userEmail) && !userEmail.equals(email) && !email.equals(""));
        //set new mail
        if (isEmailChanged) {
            user.setEmail(email);

            //set new activation code
            if (!StringUtils.isEmpty(email)) {
                user.setActivationCode(UUID.randomUUID().toString());
            }
        }
        if (!StringUtils.isEmpty(password)) {
            user.setPassword(passwordEncoder.encode(password));
        }
        userRepository.save(user);

        if (isEmailChanged) {
            sendMessage(user);
        }
    }

    public void subscribe(User currentUser, User user) {
        user.getSubscribers().add(currentUser);

        userRepository.save(user);
    }

    public void unsubscribe(User currentUser, User user) {
        user.getSubscribers().remove(currentUser);

        userRepository.save(user);
    }
}
