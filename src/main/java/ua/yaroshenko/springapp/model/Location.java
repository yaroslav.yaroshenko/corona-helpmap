package ua.yaroshenko.springapp.model;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "locations")
public class Location {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "location_id_seg")
    @SequenceGenerator(name = "location_id_seg", sequenceName = "location_seq",allocationSize = 1)
    private Long id;
    @NotBlank(message = "Latitude can't be empty")
    private String latitude;
    @NotBlank(message = "Longitude can't be empty")
    private String longitude;
    private String dimension;
    @NotNull(message = "Radius can't be empty")
    @Min(value = 1, message = "Radius should be minimum 1")
    private Double radius;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    public Location() {
    }

    public Location(String latitude, String longitude, String dimension, double radius, User user) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.dimension = dimension;
        this.radius = radius;
        this.user = user;
    }

    @Override
    public String toString() {
        return "Location{" +
                "id=" + id +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", dimension='" + dimension + '\'' +
                ", radius=" + radius +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
