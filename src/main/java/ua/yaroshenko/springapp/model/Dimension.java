package ua.yaroshenko.springapp.model;

public enum Dimension {
    METER("Meter"), KILOMETER("Kilometer");


    private final String displayValue;

    private Dimension(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}
