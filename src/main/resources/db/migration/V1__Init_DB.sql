
create sequence hibernate_sequence start 1 increment 1;
create sequence user_seq start 5 increment 1;
create sequence location_seq start 41 increment 1;

create table locations (
    id int8 not null,
    dimension varchar(255),
    latitude varchar(255),
    longitude varchar(255),
    radius float8 not null,
    user_id int8,
    primary key (id)
);

create table users (
    id int8 not null,
    activation_code varchar(255),
    email varchar(255),
    enabled boolean not null,
    filename varchar(255),
    password varchar(255) not null,
    username varchar(255) not null,
    primary key (id)
);


create table users_role (
    user_id int8 not null,
    roles varchar(255)
);


alter table locations
add constraint locations_user_id_fk
foreign key (user_id)
references users;


alter table users_role
add constraint users_role_user_id_fk
foreign key (user_id)
references users;

