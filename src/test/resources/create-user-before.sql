delete from locations;
delete from users_role;
delete from users;

insert into users(id, enabled, password, username) values
(1, true, '$2a$08$tRjZ3OjASvVn25fJMbVo8eaU9HIZ03Cl8xIY3SteZwwu0EYm1N2wG', 'admin'),
(2, true, '$2a$08$tRjZ3OjASvVn25fJMbVo8eaU9HIZ03Cl8xIY3SteZwwu0EYm1N2wG', 'user'),
(3, true, '$2a$08$tRjZ3OjASvVn25fJMbVo8eaU9HIZ03Cl8xIY3SteZwwu0EYm1N2wG', 'user1');

insert into users_role(user_id, roles) values
(1, 'ADMIN'),
(2, 'USER'),
(3, 'USER');