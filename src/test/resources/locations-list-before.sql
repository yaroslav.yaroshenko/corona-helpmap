delete from locations;

insert into locations(id, dimension, latitude, longitude, radius, user_id) values
(1, 'METER', 123, 123, 123, 1),
(2, 'KILOMETER', 23, 23, 123, 2),
(3, 'METER', 1233, 1123, 12213, 2),
(4, 'KILOMETER', 132, 331, 3, 1);

alter sequence hibernate_sequence restart with 10;